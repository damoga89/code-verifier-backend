import { Express, Request, Response } from "express";
import express from "express";
import dotenv from 'dotenv';

//Configuration the .env file
dotenv.config();

//Create Express App
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first Route of APP
app.get('/', (req: Request, res: Response)=> {
    //Send Hello Wolrd
    res.send('Welcome to my API Restful: Express + Nodemon + Jest + TS + Swagger + Mongoose');
});

//Define the first Route of APP
app.get('/hello', (req: Request, res: Response)=> {
    //Send Hello Wolrd
    res.send('Welcome to GET Route: ¿HELLO');
});

//Execute APP and Listen Request to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`)
})
